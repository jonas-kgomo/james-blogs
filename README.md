
# Jasons Blog Scraper

## Puppetter.js and Node.js, React JS


- title, slug, date, 

- body: click on read more
- pagination 



## Number of Blogs

```bash
$ node scraper/join.js
8 20
8 40
8 60
8 80
8 100
8 120
8 140
8 149
```





> returns a bloglist text, and slug/pathname

document.querySelectorAll('a.BlogList-item-title')


> array of all the titles

document.querySelectorAll('section a.BlogList-item-title')


> map 

Array.from(document.querySelectorAll('section a.BlogList-item-title')).map((blogs) => blogs.pathname)


title: text
slug: pathname

```json
(20) [
    "Cultivating Playfulness in Life", 
    "Remembering vs. Understanding", 
    "Promoting Choice in the Classroom", 
    "The Hidden Danger of "How is School Going?"", "What the "Last Day" Tells Us About School", "Centralized Authority in Schools", 
    "Why Pursue Independent Research?", 
    "The Limits of Experts", 
    "Layers of Complexity", 
    "The Limits of Routines", 
    "Passion", "Thanksgiving", "Youth and Failure", "Waiting", "Make an Ask, but Make it Specific", "You Can", "Technology Break", "Leveling Up", "Career Advice: Become an Expert", "You Are the Creative Director"]
```


date: 

Array.from(document.querySelectorAll('section div.Blog-meta')).map((blogs) => blogs.innerText)

```json
(20) ["FEBRUARY 8, 2020", "JANUARY 28, 2020", "JANUARY 26, 2020", "JANUARY 11, 2020", "JANUARY 10, 2020", "JANUARY 8, 2020", "JANUARY 5, 2020", "DECEMBER 22, 2019", "DECEMBER 15, 2019", "DECEMBER 14, 2019", "NOVEMBER 29, 2019", "NOVEMBER 28, 2019", "NOVEMBER 27, 2019", "NOVEMBER 26, 2019", "NOVEMBER 25, 2019", "NOVEMBER 24, 2019", "NOVEMBER 23, 2019", "NOVEMBER 22, 2019", "NOVEMBER 21, 2019", "NOVEMBER 20, 2019"]
```



```json
  "blogs" :[

      {
        content:'Classrooms are controlled environments.'
        title: 'Promoting Choice in the Classroom',
       date: 'JANUARY 26, 2020',
       path: '/writing/promoting-choice-in-the-classroom' 
       },
      {
        content:'Classrooms are controlled environments.'
        title: 'Promoting Choice in the Classroom',
       date: 'JANUARY 26, 2020',
       path: '/writing/promoting-choice-in-the-classroom' 
       } 
]
```