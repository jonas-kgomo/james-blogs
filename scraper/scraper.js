const puppeteer = require('puppeteer');

const fs = require('fs');
// const config = require('./config.json');



  //  await page.screenshot({path: './media/jason.png'});

  async function runScraper() {
    let browser = {};
    let page = {};
    const url = 'https://www.jamesgallagher.app';



  async function navigate() {
    browser = await puppeteer.launch({ headless: false });
    page = await browser.newPage();
    await page.goto(url);
  }





  // textContenr
  // const innerText = await page.evaluate(() => document.querySelector('p').innerText);



  /*  getResults: async (nr) => {

      let results = [];

      do { 
           let nresults = await self.data;
           results = {...results, ...nresults };

           if(results.length < nr ) {
             let nextPageButton = await self.page.$('a.BlogList-pagination-link')

             if(nextPageButton) {
               
             }
           }
      }
    }
  */


// function :clicks a butoon  wait for page to load then start scraping
// pages until nobutton shows



   async function scrapeData(){

    const blogs = await page.evaluate(() =>
    Array.from(document.querySelectorAll('section.BlogList article.BlogList-item'))
    .map((section) => ({
      title: section.querySelector('a.BlogList-item-title').text.trim(),
      date: section.querySelector('div.Blog-meta').innerText.trim(),
      path: section.querySelector('a.BlogList-item-title').pathname
      //    link: section.querySelector('a.BlogList-item-title').href,
    }))
  )

  const links = await page.evaluate(() =>
  Array.from(document.querySelectorAll('section a.BlogList-item-title')).map((blogs) => blogs.href)
)


  let data = [];
  let arr = [];

    for (var i = 0; i < blogs.length - 2; i++) {
      // console.log(blogs.length)
      //await page.screenshot({path: './media/blog.png'});
  
      await page.goto(links[i])
      await page.waitFor(2000);
      const body = await page.evaluate(() =>
  
        Array.from(document.querySelectorAll('section.Main-content div.sqs-block-content'))
        .map((section) =>
          ({
            body: section.textContent,
          })
          //
        )
      )
      // works fine for array of blogs
      data = data.concat(body);
  
      arr = data.map((item, i) => Object.assign({}, blogs[i], item));
  
      //        data[i] = data.join(',').concat(body);
      //       console.log(body)
      //      console.log(body.concat)
      //       console.log(body.concat(blogs[i]))
      // data[i] =  body.push(blogs[i], body); 
      //     data[i] =  data.push(...blogs, ...body);    
      //      data[i] =  body.concat(blogs[i]);
      //        console.log(body.concat(body))
      //      console.log(blogs.length, data.length, arr.length);
       console.log(links)
    }


    fs.writeFile('./media/datum.json', JSON.stringify(arr), err => err ? console.log(err) : null);

    
   }

   async function paginate() {
    // manually check if the next button is available or not
    const nextBtnDisabled = !!(await page.$('nav.BlogList-pagination a.BlogList-pagination-link.disabled'));
    if (!nextBtnDisabled) {
      // since it's not disable, click it
      await page.evaluate(() => document.querySelector('nav.BlogList-pagination a.BlogList-pagination-link').click());

      // just some random waiting function
      await page.waitFor(100);
      return true;
    }
    console.log({ nextBtnDisabled });
  }

  await navigate();

  // Scrape 5 pages
  for (const pageNum of [...Array(5).keys()]) {
    const title = await scrapeData();
    console.log(pageNum + 1, title);

// scroll


    await paginate();
  }
}

runScraper(); 

  //    console.log(arr)
  //console.log(blogs)
//  fs.writeFile('./media/datum.json', JSON.stringify(arr), err => err ? console.log(err) : null);
