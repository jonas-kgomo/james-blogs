const puppeteer = require('puppeteer');

const fs = require('fs');
// const config = require('./config.json');


(async () => {


  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  const url = 'https://www.jamesgallagher.app';

  // multiple urls
  const murls =  [ 
    'https://www.jamesgallagher.app/',
    'https://www.jamesgallagher.app/?offset=1574249049277',
    'https://www.jamesgallagher.app/?offset=1572433365981',
    'https://www.jamesgallagher.app/?offset=1568875670494',
    'https://www.jamesgallagher.app/?offset=1566031458450',
    'https://www.jamesgallagher.app/?offset=1564169422000',
    'https://www.jamesgallagher.app/?offset=1562353214000',
    'https://www.jamesgallagher.app/?offset=1560104519000',
    ''
  ];

  await page.goto(murls[7]);
  //  await page.screenshot({path: './media/jason.png'});

  const links = await page.evaluate(() =>
    Array.from(document.querySelectorAll('section a.BlogList-item-title')).map((blogs) => blogs.href)
  )

  const date = await page.evaluate(() =>
    Array.from(document.querySelectorAll('section div.Blog-meta')).map((blogs) => blogs.innerText)
  )


  // textContenr
  // const innerText = await page.evaluate(() => document.querySelector('p').innerText);

  const blogs = await page.evaluate(() =>
    Array.from(document.querySelectorAll('section.BlogList article.BlogList-item'))
    .map((section) => ({
      title: section.querySelector('a.BlogList-item-title').text.trim(),
      date: section.querySelector('div.Blog-meta').innerText.trim(),
      path: section.querySelector('a.BlogList-item-title').pathname
      //    link: section.querySelector('a.BlogList-item-title').href,
    }))
  )


  let data = [];
  let arr = [];




// function :clicks a butoon  wait for page to load then start scraping
// pages until nobutton shows


  for (var i = 0; i < blogs.length; i++) {
    // console.log(blogs.length)
    //await page.screenshot({path: './media/blog.png'});

    await page.goto(links[i])
    await page.waitFor(1000);
    const body = await page.evaluate(() =>
       

/* IMG HTML LINKS STyle
    Array.from(document.querySelectorAll('section.Main-content div.sqs-block-content'))
      .map((section) =>
        ({
          body: section.innerHTML
          //textContent, doesnot take images with text, innerText does not keep links
        })
        //
      )
    )
    */

      Array.from(document.querySelectorAll('section.Main-content div.sqs-layout.sqs-grid-12.columns-12'))
      .map((section) =>
        ({
          body: section.textContent
          //textContent, doesnot take images with text, innerText does not keep links
        })
        //
      )
    )
    // works fine for array of blogs
    data = data.concat(body);

    arr = data.map((item, i) => Object.assign({}, blogs[i], item));

    //        data[i] = data.join(',').concat(body);
    //       console.log(body)
    //      console.log(body.concat)
    //       console.log(body.concat(blogs[i]))
    // data[i] =  body.push(blogs[i], body); 
    //     data[i] =  data.push(...blogs, ...body);    
    //      data[i] =  body.concat(blogs[i]);
    //        console.log(body.concat(body))
  }

  console.log(blogs.length, data.length, arr.length, "link: " + murls[7])
  //    console.log(arr)
  //console.log(blogs)
  fs.writeFile('./media/data.json', JSON.stringify(arr), err => err ? console.log(err) : null);

  await browser.close();
})();